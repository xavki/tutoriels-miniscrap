#!/usr/bin/python3

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
#     wget -q https://chromedriver.storage.googleapis.com/112.0.5615.49/chromedriver_linux64.zip
#     unzip chromedriver_linux64.zip
#     mv chromedriver /usr/local/bin/
#     apt install -y -qq python3-pip chromium
#     pip install selenium
###############################################################

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import sys,os,time,re

def main():
  options = Options()
  #options.add_argument("--headless")
  options.add_argument('--no-sandbox') #no need to isolate process
  options.add_argument('--disable-dev-shm-usage') #avoid memory /dev/shm too high
  options.add_experimental_option("excludeSwitches", ['enable-automation']) #avoid browser notifcations

  browser = webdriver.Chrome(options=options)
  page = browser.get('https://fr.finance.yahoo.com/crypto-monnaies/?count=100&offset=0')
  try: 
    button = browser.find_element(By.XPATH, '//*[@id="consent-page"]/div/div/div/form/div[2]/div[2]/button[1]').click()
  except NoSuchElementException:
    pass

  max_number = browser.find_element(By.XPATH,'//*[@id="fin-scr-res-table"]/div[1]/div[1]/span[2]/span').text

  max_number = re.match(".*sur ([0-9]+$)",max_number)

  nb_values = max_number.groups()[0]

  print(nb_values)

  centaine = int(max_number.groups()[0]) / 100
  max_number = int(centaine) + 1

  log_count = 0

  for x in range(0, max_number):
    time.sleep(2)
    offset = x * 100
    print(offset)
    browser.get('https://fr.finance.yahoo.com/crypto-monnaies/?count=100&offset=' + str(offset) + '&guccounter=1')
    names = browser.find_elements(By.XPATH,'//*[@data-test="quoteLink"]')
    for name in names:
      log_count += 1
  
  if log_count == int(nb_values):
    print("Success : " + str(log_count) + " values scraped")
  else:
    print("Error : Less values scraped than expected")
    sys.exit(1)

  time.sleep(600)
  



if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)