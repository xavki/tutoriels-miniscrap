#!/usr/bin/python3

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
#     wget -q https://chromedriver.storage.googleapis.com/112.0.5615.49/chromedriver_linux64.zip
#     unzip chromedriver_linux64.zip
#     mv chromedriver /usr/local/bin/
#     apt install -y -qq python3-pip chromium
#     pip install selenium
###############################################################

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import sys,os,time,re

import pika,json
from datetime import datetime

# Connection Rabbitmq
credentials = pika.PlainCredentials("xavki", "password")
connection = pika.BlockingConnection(pika.ConnectionParameters("192.168.13.203","5672","/",credentials))
channel = connection.channel()
channel.queue_declare(queue="cryptos.target",durable=True)

# Class
class Message():

  def __init__(self, created_at, runts, code, title):
    self.runts = runts
    self.code = code
    self.title = title
    self.created_at = created_at

  def getCode(self):
    return self.code

  def getTitle(self):
    return self.title

  def getCreatedAt(self):
    return self.created_at

  def getRunts(self):
    return self.runts

  def jsonMessage(self):
    message = { "createdAt": self.created_at, "runts": self.runts, "code": self.code, "title": self.title }
    print(message)
    message = json.dumps(message)
    return message

  def pushToQueue(self):
    channel.basic_publish(exchange="cryptos.target",
                 routing_key='cryptos',
                 properties=pika.BasicProperties(
                   delivery_mode = 2
                 ),
                 body=self.jsonMessage())

def main():
  options = Options()
  #options.add_argument("--headless")
  options.add_argument('--no-sandbox') #no need to isolate process
  options.add_argument('--disable-dev-shm-usage') #avoid memory /dev/shm too high
  options.add_experimental_option("excludeSwitches", ['enable-automation']) #avoid browser notifcations

  browser = webdriver.Chrome(options=options)
  page = browser.get('https://fr.finance.yahoo.com/crypto-monnaies/?count=100&offset=0')
  try: 
    button = browser.find_element(By.XPATH, '//*[@id="consent-page"]/div/div/div/form/div[2]/div[2]/button[1]').click()
  except NoSuchElementException:
    pass

  max_number = browser.find_element(By.XPATH,'//*[@id="fin-scr-res-table"]/div[1]/div[1]/span[2]/span').text

  max_number = re.match(".*sur ([0-9]+$)",max_number)

  nb_values = max_number.groups()[0]

  print(nb_values)

  centaine = int(max_number.groups()[0]) / 100
  max_number = int(centaine) + 1

  log_count = 0

  for x in range(0, max_number):
    time.sleep(2)
    offset = x * 100
    print(offset)
    browser.get('https://fr.finance.yahoo.com/crypto-monnaies/?count=100&offset=' + str(offset) + '&guccounter=1')
    cryptos = browser.find_elements(By.XPATH,'//*[@data-test="quoteLink"]')
    runts = json.dumps(datetime.now(),default=str) #

    for crypto in cryptos:
      log_count += 1
      message = Message(time.time(), runts, crypto.text, crypto.get_attribute("title")) #
      message.pushToQueue() #
  
  if log_count == int(nb_values):
    print("Success : " + str(log_count) + " values scraped")
  else:
    print("Error : Less values scraped than expected")
    sys.exit(1)

  time.sleep(600)
  



if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)