#!/usr/bin/python3

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
###############################################################

import sys,os,time,re
import pika,json


credentials = pika.PlainCredentials("xavki", "password")
connection = pika.BlockingConnection(pika.ConnectionParameters("192.168.13.203","5672","/",credentials))
channel = connection.channel()
channel.queue_declare(queue="cryptos.target",durable=True)

#conn = psycopg2.connect(
#  database="cryptoslist",
#  user="cryptos",
#  password="password",
#  host="192.168.13.204",
#  port="5432"
#)


# Let's Go !! #################################################

def main():

    #cursor = conn.cursor()
    #cursor.execute("CREATE TABLE IF NOT EXISTS cryptos_ref (code varchar(255) PRIMARY KEY, date_create timestamp, title varchar(255),scrape_begin timestamp, scrape_end timestamp )")
    #conn.commit()


    def callback(ch, method, properties, body):
      data = json.loads(body)
      sql = "INSERT INTO cryptos_ref (code, date_create,title) VALUES(%s,%s,%s) ON CONFLICT (code) DO NOTHING;"
      value = (data['code'],data['runts'],data['title'])
      print(value)
      #cursor.execute(sql,value)
      #conn.commit()

    channel.basic_consume(queue="cryptos.target",on_message_callback=callback,auto_ack=True)
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
      try:
          sys.exit(0)
      except SystemExit:
          os._exit(0)

